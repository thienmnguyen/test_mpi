#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) 
{ 
	int world_size, universe_size = 0, *universe_sizep, rank, flag, rc;
	MPI_Comm everyone;	/* intercommunicator */

	MPI_Init(&argc, &argv);
	MPI_Comm_size(MPI_COMM_WORLD, &world_size);
	if (world_size != 1) 
    {
		printf("FAILURE: Started %d master processes. Please only start 1 master process.\n", world_size);
		exit(1);
	}

	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	printf("Master rank:%d\n",rank);

	/* NOTE: Ideally MPI_UNIVERSE_SIZE would be the size of the job
	 * allocation. Presently it is the size of the job step allocation.
	 * In any case, additional tasks can be spawned */
	MPI_Comm_get_attr(MPI_COMM_WORLD, MPI_UNIVERSE_SIZE, &universe_sizep, &flag);
	// Somehow, the MPI_UNIVERSE_SIZE is always 1 (MPI_Comm_size of this master)
	if (flag) 
    {
		universe_size = *universe_sizep;
		printf("MPI_UNIVERSE_SIZE is %d\n", universe_size);
	}
	
	// Anyway, just launch our child processes.
	// Note: if using SLURM, make sure we set "#SBATCH -n" greater than the total
	// number of processes that we need (both for master and children)	
	if (universe_size < 2)
    {
		universe_size = 16;
    }

    // Start slave process
	rc = MPI_Comm_spawn("./slave", MPI_ARGV_NULL, universe_size - 1,  
			    MPI_INFO_NULL, 0, MPI_COMM_SELF, &everyone,  
			    MPI_ERRCODES_IGNORE);
	if (rc != MPI_SUCCESS) 
    {
		printf("FAILURE: MPI_Comm_spawn(): %d\n", rc);
		exit(1);
	}

	MPI_Finalize();
	exit(0);
} 