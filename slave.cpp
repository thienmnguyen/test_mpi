#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/utsname.h>

#define COMM_TAG 1000

static void pass_its_neighbor(const int rank, const int size, const int* buf)
{
	struct utsname uts;
	MPI_Request request[2];
	MPI_Status status[2];

	MPI_Irecv((void *)buf, 1, MPI_INT, ((rank+size-1)%size), COMM_TAG,
		MPI_COMM_WORLD, &request[0]);
	MPI_Isend((void *)&rank, 1, MPI_INT, ((rank+1)%size), COMM_TAG,
		MPI_COMM_WORLD, &request[1]);
	MPI_Waitall(2, request, status);

	uname(&uts);
	fprintf(stdout, "Rank[%d] on %s just received msg from Rank %d\n",
		rank, uts.nodename, *buf);
}

int main(int argc, char *argv[]) 
{ 
	int buf, size, rank, rc = 0;
	MPI_Comm parent;

	MPI_Init(&argc, &argv);
	MPI_Comm_get_parent(&parent);
	
    const auto finish = [&rc](){
        MPI_Finalize(); 
	    exit(rc);
    };

    
    // This is a slave process, hence must be started via MPI_Comm_spawn,
    // i.e. MPI_Comm_get_parent must return a valid parent.
    if (parent == MPI_COMM_NULL) 
    {
		puts("No parent!");
		rc = 1;
		finish();
	}

	MPI_Comm_remote_size(parent, &size);
	
    if (size != 1) 
    {
		puts("Something's wrong with the parent");
		rc = 2;
		finish();
	}

	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	printf("slave rank:%d size:%d\n", rank, size);
 
	buf = rank;	
	pass_its_neighbor(rank, size, &buf);
    finish();
} 